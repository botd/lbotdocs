#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup

def readme():
    with open('README.rst') as file:
        return file.read()

setup(
    name='lbotdocs',
    version='1',
    url='https://bitbucket.org/bthate/lbotdocs',
    author='Bart Thate',
    author_email='bthate@dds.nl',
    description="documentation for lbot",
    long_description=readme(),
    long_description_content_type="text/markdown",
    license='Public Domain',
    zip_safe=True,
    install_requires=["lbot",],
    classifiers=['Development Status :: 3 - Alpha',
                 'License :: Public Domain',
                 'Operating System :: Unix',
                 'Programming Language :: Python',
                 'Topic :: Utilities'
                ]
)
