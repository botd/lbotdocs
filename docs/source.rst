.. image:: jpg/bart2.jpg
    :height: 3.5cm
    :width: 100%

.. title:: IRC channel daemon

.. _source:

S O U R C E
###########

LBOT contains the following modules:

.. autosummary::
    :toctree:
    :template: module.rst

    lo                          - object library.
    lo.clk                      - clock functions.
    lo.dbs                      - database.
    lo.evt                      - basic event.
    lo.gnr                      - generic object functions.
    lo.hdl                      - handler.
    lo.ldr                      - module loader.
    lo.tms                      - time related functions.
    lo.thr                      - threads.
    lo.typ                      - typing.
    lbot.cfg                    - config command.
    lbot.cmd                    - list of commands.
    lbot.irc                    - irc bot.
    lbot.rss                    - feed fetcher.
    lbot.shl                    - shell code.
    lbot.shw                    - show runtime.
    lbot.udp                    - udp to irc relay
    lbot.usr                    - user management.


have fun coding ;]
